FROM node:12.17-alpine3.11 as build-step
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build --prod

FROM nginx:1.18.0-alpine as prod-state
COPY --from=build-step /app/dist/angular9-demo /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

# BUILD docker build -t angular .
# RUN docker run -p 80:80 angular
