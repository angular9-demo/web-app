import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { PostModel } from './post.model';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private endpoint = environment.BASE_URL + "/api/posts";
  private postListener = new Subject<
    {posts?: PostModel[], post?: PostModel, error?: string}>();

  constructor(private http: HttpClient,
    private router: Router) { }

  getPostListener() {
    return this.postListener.asObservable();
  }

  getPosts() {
    this.http.get<PostModel[]>(this.endpoint)
      .subscribe((response: PostModel[] )=> {
        this.postListener.next({posts: response});
      }, error => this.postListener.next({error: error.error.message}));
  }

  getPost(id: string) {
    this.http.get<PostModel>(this.endpoint + "/" + id)
      .subscribe(response=> {
        this.postListener.next({post: response});
      }, error => {
        this.router.navigate(['/posts']);
      });
  }

  savePost(postModel: PostModel) {
    this.http.post<{message: string, id: number}>(this.endpoint, postModel)
      .subscribe(response => {
          this.router.navigate(['/posts/view', response.id]);
      }, error => this.postListener.next({error: error.error.message}));
  }

  deletePost(id: number) {
    this.http.delete<{message: string}>(this.endpoint + "/" + id)
      .subscribe(response => {
        this.getPosts();
        this.router.navigate(['/posts']);
      }, error => this.postListener.next({error: error.error.message}));
  }
}
