import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostModel } from '../post.model';
import { UserModel } from 'src/app/auth/user.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/auth/user.service';
import { PostService } from '../post.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

  isLoading = false;
  posts: PostModel[];
  postListenerSub: Subscription;
  userModel = {} as UserModel;
  constructor(private userService: UserService,
    private postService: PostService,
    private title: Title) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.title.setTitle("List of Posts | Demo");
    this.userModel = this.userService.getSavedUserData();
    this.getPosts();
  }
  ngOnDestroy() {
    this.postListenerSub.unsubscribe();
  }

  getPosts() {
    this.postService.getPosts();
    this.postListenerSub = this.postService.getPostListener()
      .subscribe(response => {
        this.posts = response.posts;
        this.isLoading = false;
      });

  }

  onDelete(id: number) {
    const confirm = window.confirm("Are you sure you want to delete post?");
    if(confirm) {
      this.isLoading = true;
      this.postService.deletePost(id);
    }
  }
}
