import { Component, OnInit } from '@angular/core';
import { PostModel } from '../post.model';
import { ActivatedRoute } from '@angular/router';
import { UserModel } from 'src/app/auth/user.model';
import { UserService } from 'src/app/auth/user.service';
import { PostService } from '../post.service';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.css']
})
export class PostViewComponent implements OnInit {

  isLoading = false;
  post = {} as PostModel;
  userModel = {} as UserModel;
  postListenerSub: Subscription;
  error: string;

  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private postService: PostService,
    private title: Title) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.title.setTitle("Post Details | Demo");
    this.userModel = this.userService.getSavedUserData();
    const id = this.route.snapshot.paramMap.get('id');
    this.postService.getPost(id);
    this.postListenerSub = this.postService.getPostListener()
      .subscribe(response => {
        if(response.post) {
          this.post = response.post;
          this.isLoading = false;
        }
    });
  }


  onDelete(id: number) {
    const confirm = window.confirm("Are you sure you want to delete post?");
    if(confirm) {
      this.isLoading = true;
      this.postService.deletePost(id);
    }
  }

}
