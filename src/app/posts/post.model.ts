import { UserModel } from '../auth/user.model';

export interface PostModel {
  id?: number;
  title: string;
  content: string;
  author: UserModel;
}
