import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PostModel } from '../post.model';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { UserService } from 'src/app/auth/user.service';
import { UserModel } from 'src/app/auth/user.model';
import { Subscription } from 'rxjs';
import { PostService } from '../post.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit, OnDestroy {

  isLoading = false;
  postForm: FormGroup;
  componentTitle = "Create Post";
  postId: string;
  mode = "create";
  clicked = false;
  post = {} as PostModel;
  userModel: UserModel;
  postListenerSub: Subscription;

  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private postService: PostService,
    private title: Title) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.userModel = this.userService.getSavedUserData();
    this.postFormInit();
    this.createOrEdit();
  }

  ngOnDestroy() {
  }

  postFormInit() {
    this.postForm = new FormGroup({
      title : new FormControl(null,
        {validators: [Validators.required, Validators.minLength(5)]}),
      content : new FormControl(null,
        {validators: [Validators.required]})
    });
  }

  createOrEdit(){
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('id')) {
        this.title.setTitle("Edit Post | Demo");
        this.mode = "edit";
        this.postId = paramMap.get('id');
        this.componentTitle = "Edit Post";

        this.postService.getPost(this.postId);
        this.postListenerSub = this.postService.getPostListener()
          .subscribe(response => {
            if(response.post) {
              this.post = response.post;
              this.postForm.setValue({
                title: this.post.title,
                content: this.post.content,
              });
              this.isLoading = false;
            }
          })
      }
      else {
        this.title.setTitle("Create Post | Demo");
        this.mode = "create";
        this.postId = null;
        this.isLoading = false;
      }
    })
  }

  onSavePost() {
    if(this.postForm.invalid) return;

    const postModel: PostModel = {
      id: +this.postId,
      title: this.postForm.value.title,
      content: this.postForm.value.content,
      author: this.userModel
    }
    this.clicked = true;
    this.postService.savePost(postModel);
  }

}
