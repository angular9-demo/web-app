import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthModel } from './auth.model';
import { UserModel } from './user.model';
import { Subject } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate{

  private signupEndpoint = environment.BASE_URL + "/api/users/signup";
  private loginEndpoint = environment.BASE_URL + "/api/login";

  private authStatusListener = new Subject<
    {status?: boolean, error?:string, user?:UserModel}>();
  private userModel: UserModel;

  constructor(private http: HttpClient,
    private router: Router) { }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getUser

  login(auth: AuthModel) {
    this.http.post(this.loginEndpoint, auth)
      .subscribe((response: any) => {
        const token = response.Authorization.replace("Bearer ", "");
        const decodedToken = jwt_decode(token);
        this.userModel = decodedToken.sub;
        this.saveDataToLocal(response.Authorization, decodedToken.sub);
        this.authStatusListener.next({status:true, user:this.userModel});
        this.router.navigate(["/posts"]);
      }, error => this.authStatusListener.next({status: false}));
  }

  signup(user: UserModel) {
    this.http.post(this.signupEndpoint, user)
      .subscribe(response => {
        this.router.navigate(["/login"]);
      }, error =>
        this.authStatusListener.next({error: error.error.message, status:false}));
  }


  getSavedUserData() {
    const userModel = localStorage.getItem("userModel");
    if(userModel) {
      this.userModel = JSON.parse(userModel);
      this.authStatusListener.next({status: true, user:this.userModel});
      return this.userModel;
    }
  }

  logout() {
    localStorage.removeItem("userModel");
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }

  private saveDataToLocal(token: string, userModel: string) {
    localStorage.setItem('userModel', userModel);
    localStorage.setItem('token', token);
  }

  canActivate() {
    if(this.getSavedUserData() === undefined) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
