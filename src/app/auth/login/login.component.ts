import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthModel } from '../auth.model';
import { Subscription } from 'rxjs';
import { UserService } from '../user.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginError = false;
  authStatusSub: Subscription;
  constructor(private userService: UserService,
    private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle("Login | Demo");
    this.authStatusSub = this.userService.getAuthStatusListener()
      .subscribe(response => {
        this.loginError = !response.status;
      });
  }
  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

  onLogin(form: NgForm) {
    if(form.invalid) return;

    const authModel: AuthModel = {
      username: form.value.username,
      password: form.value.password,
    }
    this.userService.login(authModel);
  }
}
