import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserModel } from '../user.model';
import { UserService } from '../user.service';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {

  authStatusSub: Subscription;
  errorMessage: string;
  signUpForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private title: Title) {

    this.signUpForm = formBuilder.group({
      username: new FormControl('',
        {validators: [Validators.required, Validators.minLength(3), Validators.maxLength(8)]}),
      password: new FormControl('',
        {validators: [Validators.minLength(6), Validators.maxLength(12), Validators.required]}),
      firstName: new FormControl('',
        {validators: [Validators.minLength(2), Validators.required]}),
      lastName: new FormControl('',
      {validators: [Validators.minLength(2), Validators.required]})
    });
  }

  ngOnInit(): void {
    this.title.setTitle("Create Account | Demo");
    this.authStatusSub = this.userService.getAuthStatusListener()
      .subscribe(response => {
        this.errorMessage = response.error;
      });
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

  onSignUp() {
    if(this.signUpForm.invalid) return;

    const userModel: UserModel = {
      username: this.signUpForm.value.username,
      password: this.signUpForm.value.password,
      firstName: this.signUpForm.value.firstName,
      lastName: this.signUpForm.value.lastName
    }
    this.userService.signup(userModel);
  }
}
