import { AuthModel } from './auth.model';

export interface UserModel extends AuthModel {
  id?: number;
  firstName: string;
  lastName: string;
}
