import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/auth/user.service';
import { UserModel } from 'src/app/auth/user.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  isHeaderCollapsed = true;
  userModel: UserModel;
  name: string;
  isLoggedIn = false;
  authStatusSub: Subscription;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.authStatusSub = this.userService.getAuthStatusListener()
      .subscribe(response => {
        this.isLoggedIn = response.status;
        if(response.user) {
          this.userModel = response.user;
          this.name = this.userModel.firstName + " " + this.userModel.lastName;
        }
      });
  }
  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

  onLogout() {
    this.isLoggedIn = false;
    this.userService.logout();
  }
}
